import { withRouter } from 'react-router-dom';
import Nav from "../components/Nav/Nav";
import StatisticsContainer from "../components/Statistics/Statistics.container";
import { GET_STATISTICS_STATE } from "../graphql/Query";

const State = ({ match }) => {
  return (
    <div>
      <s-row>
        <s-col span="2"></s-col>
        <s-col span="8">
          <Nav />
          <StatisticsContainer getData={GET_STATISTICS_STATE} dataTitle="getStateData" search={match.params.state} />
        </s-col>
        <s-col span="2"></s-col>
      </s-row>
    </div>
  );
}

export default withRouter(State);