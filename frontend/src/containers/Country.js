import { useLazyQuery, useMutation } from '@apollo/client';
import { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import Nav from "../components/Nav/Nav";
import StatisticsContainer from "../components/Statistics/Statistics.container";
import TableContainer from "../components/Table/Table.container";
import "./Country.scss";
import { GET_STATISTICS_COUNTRY, GET_FAV, GET_INDIA_DATA } from "../graphql/Query";
import { SET_FAV } from "../graphql/Mutation";

const Country = ({ match }) => {

  const uid = localStorage.getItem("uid");

  const [setFav] = useMutation(SET_FAV);
  const [isFav, setIsFav] = useState(false);
  const [getFav, { data }] = useLazyQuery(GET_FAV);

  useEffect(() => {
    if(uid) {
      getFav({
        variables: {
          uid
        }
      })
    }
  }, []);

  useEffect(() => {
    if(data?.getFav.country === match.params.country) {
      setIsFav(true);
    }
  }, [data])

  const favHandler = () => {
    if(uid) {
      if(isFav) {
        setFav({
          variables: {
            data: {
              uid,
              type: "country",
              value: 'none'
            }
          }
        });
        setIsFav(false);
      } else {
        setFav({
          variables: {
            data: {
              uid,
              type: "country",
              value: match.params.country
            }
          }
        })
        setIsFav(true);
      }
    } else {
      alert("Please Login to select your favorites")
    }
  }

  return (
    <div className="Country">
      <s-row>
        <s-col span="2"></s-col>
        <s-col span="8">
          <Nav />
          <div className="top1 pad-right-md">
            <h1 className="pad-left-sm">{match.params.country}</h1>
            <s-icon onClick={favHandler} class="medium" 
              name={isFav ? "star-filled" : "star"}>
            </s-icon>
          </div>

          <StatisticsContainer getData={GET_STATISTICS_COUNTRY} dataTitle="getCountryData" search={match.params.country} />
          {
              match.params.country === 'India' ?
              <TableContainer getData={GET_INDIA_DATA} dataTitle="getIndiaData" title="State" />
              : null
          }  
        </s-col>
        <s-col span="2"></s-col>
      </s-row>
    </div>
  );
}

export default withRouter(Country);