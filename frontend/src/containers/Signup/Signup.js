import { useMutation, useApolloClient } from '@apollo/client';
import { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import Nav from "../../components/Nav/Nav";
import "./Signup.scss";
import { SIGNUP_USER } from "../../graphql/Mutation";
import { LOCAL_UID } from "../../graphql/cache";

const Signup = ({history}) => {

    const client = useApolloClient();

    const [signupUser, {data}] = useMutation(SIGNUP_USER);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassowrd, setConfirmPassowrd] = useState("");

    useEffect(() => {
        if(data && data.signupUser) {
            client.writeQuery({
                query: LOCAL_UID,
                data: {
                    uid: data.signupUser
                }
            });
            localStorage.setItem("uid", data.signupUser);
            history.push('/');
        }
    }, [data, history, client])

    const onSubmitHandler = (e) => {
        console.log(email, password, confirmPassowrd);
        e.preventDefault();
        if(password === confirmPassowrd) {
            signupUser({ variables: { data: {
                email,
                password
            } } })
        } else {
        alert('password and confirmPassowrd are not same');
        }
    }

    return (
        <div className="Signup">
        <Nav />
        <s-row>
            <s-col span="4 sm-2 md-2"></s-col>
            <s-col span="4 sm-8 md-8">
                <s-box class="margin-top-lg pad-left-lg pad-right-lg">
                <h2>Sign Up</h2>
                <form onSubmit={onSubmitHandler}>
                    <input type="email" placeholder="email" onChange={(e) => setEmail(e.target.value)} />
                    <input type="password" placeholder="password" onChange={(e) => setPassword(e.target.value)} />
                    <input type="password" placeholder="confirm Passowrd" onChange={(e) => setConfirmPassowrd(e.target.value)} />
                    <button type="submit" className="primary margin-top-sm">Submit</button>
                </form>
                </s-box>
            </s-col>
            <s-col span="4"></s-col>
            </s-row> 
        </div>
    );
}

export default withRouter(Signup);
