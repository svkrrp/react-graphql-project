import { useApolloClient, useMutation } from '@apollo/client';
import { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import Nav from "../../components/Nav/Nav";
import "./Login.scss";
import { LOGIN_USER } from "../../graphql/Mutation";
import { LOCAL_UID } from "../../graphql/cache";

const Login = ({history}) => {

  const client = useApolloClient();

  const [loginUser, {data}] = useMutation(LOGIN_USER);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    if(data && data.loginUser) {
      client.writeQuery({
        query: LOCAL_UID,
        data: {
          uid: data.loginUser
        }
      });
      localStorage.setItem("uid", data.loginUser);
      history.push('/');
    }
  }, [data, history, client]);

  const onSubmitHandler = (e) => {
    console.log(email, password);
    e.preventDefault();
    if(email && password) {
      loginUser({ variables: { data: {
        email,
        password
      }}})
    } else {
      alert('password and confirmPassowrd are not same');
    }
  }

  return (
    <div className="Login">
      <Nav />
      <s-row>
          <s-col span="4 sm-2 md-2"></s-col>
          <s-col span="4 sm-8 md-8">
            <s-box class="margin-top-lg pad-left-lg pad-right-lg">
              <h2>Login</h2>
              <form onSubmit={onSubmitHandler}>
                <input type="email" placeholder="email" onChange={(e) => setEmail(e.target.value)} />
                <input type="password" placeholder="password" onChange={(e) => setPassword(e.target.value)} />
                <button type="submit" className="primary margin-top-sm">Submit</button>
              </form>
              <Link to="/signup"><button className="primary margin-top-sm">Signup</button></Link>
            </s-box>
          </s-col>
          <s-col span="4"></s-col>
        </s-row> 
    </div>
  );
}

export default withRouter(Login);
