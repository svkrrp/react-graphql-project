import { useApolloClient, useLazyQuery } from '@apollo/client';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Nav from "../components/Nav/Nav";
import StatisticsContainer from "../components/Statistics/Statistics.container";
import TableContainer from "../components/Table/Table.container";
import { GET_COUNTRIES_DATA, GET_STATISTICS, GET_FAV } from "../graphql/Query";
import { LOCAL_UID } from "../graphql/cache";

const Home = () => {
  const client = useApolloClient();

  const [uid, setUid] = useState(null);
  const [favCountry, setFavCountry] = useState("");
  const [getFav, { data }] = useLazyQuery(GET_FAV);

  useEffect(() => {
    const localUid = localStorage.getItem("uid");
    if(localUid) {
      setUid(localUid);
    }
    const res = client.readQuery({ query: LOCAL_UID });
    if(res && res.uid) {
      setUid(res.uid);
    }
  }, [])

  useEffect(() => {
    if(uid) {
      getFav({
        variables: {
          uid
        }
      })
    }
  }, [uid, getFav])

  useEffect(() => {
    if(data && data.getFav) {
      setFavCountry(data.getFav.country);
    }
  }, [data])

  return (
    <div>
      <s-row>
        <s-col span="2"></s-col>
        <s-col span="8">
          <Nav />
          {
            favCountry && favCountry !== 'none' ?
            <h2>Your Favorite Country - <Link to={`/${favCountry}`}>{favCountry}</Link></h2>
            : <s-alert status="info">Select Your Favorite Country To Show up here</s-alert>
          }
          <StatisticsContainer getData={GET_STATISTICS} dataTitle="getWorldData" />
          <TableContainer getData={GET_COUNTRIES_DATA} dataTitle="getCountriesData" title="Country" />
        </s-col>
        <s-col span="2"></s-col>
      </s-row>
    </div>
  );
}

export default Home;