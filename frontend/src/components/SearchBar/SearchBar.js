import './SearchBar.scss';

const SearchBar = ({searchHandler}) => {
  return (
    <div className="SearchBar margin-top-xl">
      <input type="search" placeholder="Search Here" onChange={searchHandler} />
    </div>
  );
}

export default SearchBar;