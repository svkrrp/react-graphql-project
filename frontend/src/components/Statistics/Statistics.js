/* eslint-disable no-unused-vars */
import { Component } from "react";
import './Statistics.scss';

class Statistics extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false
    }
  }

  toggleHandler = () => { 
    this.setState((prevState, prevProps) => ({
      isExpanded: !prevState.isExpanded
    }))
  }

  render() {
    const { total, recovered, deceased, infected } = this.props.covidCounts;
    const { isExpanded } = this.state;

    return (
      <div className="Statistics margin-top-xl">
        <s-box>
          <div className="top">
            <div className="container pad-left-sm pad-right-sm">
              <div>
                <h2>Total COVID-19 cases</h2>
                <h2 className="text-md">{ total } people have been affected</h2>
              </div>
              <s-icon onClick={this.toggleHandler} name={isExpanded ? "chevron-up" : "chevron-down"} role="img"></s-icon>
            </div>
            {
              isExpanded ?
              <div className="bottom">
                <s-box class="infected">
                  <h2>Currently Infected</h2>
                  <h3>{infected}</h3>
                </s-box>
                <s-box class="recovered">
                  <h2>Total Recovered</h2>
                  <h3>{recovered}</h3>
                </s-box>
                <s-box class="deceased">
                  <h2>Total Deceased</h2>
                  <h3>{deceased}</h3>
                </s-box>
              </div> 
              : null
            }
          </div>
        </s-box>
      </div>
    );
  }
}

export default Statistics;