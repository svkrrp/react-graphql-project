import { useQuery } from '@apollo/client';
import Loader from "../Loader/Loader";
import Error from "../Error/Error";
import Statistics from "./Statistics";

const StatisticsContainer = ({ getData, dataTitle, search }) => {

  const { loading, error, data } = useQuery(getData, {
    variables: { search }
  });

  if(loading) {
    return <Loader />
  }
  if(error) return <Error />

  return (
    <div>
      <Statistics title="Country" covidCounts={data[dataTitle]} />
    </div>
  );
}

export default StatisticsContainer;