import { useApolloClient } from '@apollo/client';
import { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import './Nav.scss';
import { LOCAL_UID } from "../../graphql/cache";

const Nav = ({history}) => {

  const client = useApolloClient();

  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const getLocalUid = localStorage.getItem("uid");
    console.log("getlocaluid", getLocalUid);
    if(getLocalUid) {
      client.writeQuery({
        query: LOCAL_UID,
        data: {
          uid: getLocalUid
        }
      });
      console.log("uid", getLocalUid);
    }
    const res = client.readQuery({ query: LOCAL_UID });
    if(res && res.uid) { 
      setIsLoggedIn(true);
    }
  }, [])

  const onClickhandler = () => {
    if(isLoggedIn) {
      client.writeQuery({
        query: LOCAL_UID,
        data: {
          uid: null
        }
      });
      setIsLoggedIn(false);
      localStorage.removeItem("uid");
    } else {
      history.push('/login');
    }
  }

  return (
    <div className="Nav pad-all-md pad-left-xl">
      <h1 onClick={() => history.push('/')}>Covid Dashboard</h1>
      <h3 onClick={onClickhandler}>
        {
          isLoggedIn ? "Logout" : "Login" 
        }
      </h3>
    </div>
  );
}

export default withRouter(Nav);