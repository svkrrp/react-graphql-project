import { useQuery } from '@apollo/client';
import Loader from "../Loader/Loader";
import Table from "./Table";
import Error from "../Error/Error";

const TableContainer = ({ getData, dataTitle, title }) => {

  const { loading, error, data } = useQuery(getData);
  
  if(loading) {
    return <Loader />
  }
  if(error) return <Error />

  return (
    <div>
      <Table title={title} list={data[dataTitle]} />
    </div>
  );
}

export default TableContainer;