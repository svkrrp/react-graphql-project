import { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import SearchBar from "../SearchBar/SearchBar";

const Table = ({ title, list, history, match }) => {

  const [tempList, setTempList] = useState([]);

  const searchHandler = (e) => {
    const filteredList = list.filter(l => l[title.toLowerCase()].toLowerCase().includes(e.target.value));
    setTempList(filteredList);
  }
  
  useEffect(() => {
    setTempList(list);
  }, []);

  return (
    <div className="margin-top-xl">
      <div className="margin-bottom-lg">
        <SearchBar searchHandler={searchHandler} />
      </div>
      <s-table-container tabindex="0">
          <table>
              <thead>
                  <tr style={{
                    backgroundColor: "white"
                  }}>
                      <th>{ title }</th>
                      <th>Total Infected</th>
                      <th>Total Recovered</th>
                      <th>Total Deceased</th>
                  </tr>
              </thead>
              <tbody>
                {
                  tempList.map((l, i) => {
                    return (
                      <tr key={i}
                        onClick={() => history.push(match.params.country ? `${match.params.country}/${l.state}` : l.country)}
                      >
                        <th>{l[title.toLowerCase()]}</th>
                        <th>{l.infected}</th>
                        <th>{l.recovered}</th>
                        <th>{l.deceased}</th>
                      </tr>
                    );
                  })
                }
              </tbody>
          </table>
      </s-table-container>
    </div>
  );
}

export default withRouter(Table);
