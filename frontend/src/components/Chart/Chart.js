import React, { Component } from 'react';
import CanvasJSReact from '../../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Chart extends Component {	
  render() {
    console.log("data",this.props.data[0]);

    const recoveredData = [];
    const deathsData = [];
    const activeCasesData = [];

    this.props.data.forEach((data, i) => {
      recoveredData.push({
        x: i,
        y: parseInt(data.recovered)
      })
      activeCasesData.push({
        x: i,
        y: data.totalCases
      })
    });

    console.log(recoveredData, deathsData, activeCasesData);

		const options = {
			animationEnabled: true,
			exportEnabled: true,
			theme: "light2", // "light1", "dark1", "dark2"
			title:{
				text: "History data"
			},
			axisY: {
				title: "statistics",
				suffix: ""
			},
			axisX: {
				title: "Days ------>",
				prefix: "W",
				interval: 2
			},
			data: [{
				type: "line",
				toolTipContent: "Total {x}: {y}",
				dataPoints: activeCasesData
      },
      {
				type: "line",
				toolTipContent: "Recovered {x}: {y}",
				dataPoints: recoveredData
			}]
		}
		return (
		<div className="margin-top-xl">
			<CanvasJSChart options = {options}
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}

export default Chart;