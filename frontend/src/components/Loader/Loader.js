const Loader = () => {
  return (
    <s-loader loading="" aria-live="polite">
      <span className="visually-hidden loading-message">Content is loading...</span>
    </s-loader>
  )
}

export default Loader;