/* eslint-disable no-undef */
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Statistics from '../components/Statistics/Statistics';

configure({adapter: new Adapter()});

describe('Statistics.js', () => {
  it('Statistics component', () => {
    const covidCounts = {
      total: 1000, 
      totalRecovered: 900, 
      totalDeceased: 2, 
      currentlyInfected: 8
    }
    expect(shallow(<Statistics covidCounts={covidCounts} />)).toMatchSnapshot();
  });

  it('checks toggle', () => {
    const covidCounts = {
      total: 1000, 
      totalRecovered: 900, 
      totalDeceased: 2, 
      currentlyInfected: 8
    }
    const wrapper = shallow(<Statistics covidCounts={covidCounts} />);
    expect(wrapper.state()).toEqual({isExpanded: false});
    wrapper.find('s-icon').simulate('click');
    expect(wrapper.state()).toEqual({isExpanded: true});
  })
});
