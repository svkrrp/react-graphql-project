/* eslint-disable no-undef */
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SearchBar from '../components/SearchBar/SearchBar';

configure({adapter: new Adapter()});

it('Search bar card component', () => {
  expect(shallow(<SearchBar />)).toMatchSnapshot();
})
