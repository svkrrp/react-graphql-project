/* eslint-disable no-undef */
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Loader from '../components/Loader/Loader';

configure({adapter: new Adapter()});

it('Loader component', () => {
  expect(shallow(<Loader />)).toMatchSnapshot();
})
