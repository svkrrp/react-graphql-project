import { gql } from '@apollo/client';

export const SET_FAV = gql`
    mutation SetFav($data: CreateFavInput!) {
        setFav(data: $data)
    }
`;

export const SIGNUP_USER = gql`
    mutation SignupUser($data: CreateUserInput!) {
        signupUser(data: $data)
    }
`;

export const  LOGIN_USER = gql`
    mutation LoginUser($data: CreateUserInput!) {
        loginUser(data: $data)
    }
`;