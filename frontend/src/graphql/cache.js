import { gql } from "@apollo/client";

export const LOCAL_UID = gql`
  query uid {
    uid
  }
`;
