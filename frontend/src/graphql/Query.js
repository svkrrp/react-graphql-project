import { gql } from '@apollo/client';

export const GET_STATISTICS = gql`
    query getWorldData {
        getWorldData {
            total
            infected
            recovered
            deceased
        }
    }
`;

export const GET_STATISTICS_COUNTRY = gql`
    query getCountryData($search: String!) {
        getCountryData(country: $search) {
            total
            infected
            recovered
            deceased
        }
    }
`;

export const GET_STATISTICS_STATE = gql`
    query getStateData($search: String!) {
        getStateData(state: $search) {
            total
            infected
            recovered
            deceased
        }
    }
`;

export const GET_COUNTRIES_DATA = gql`
    query getCountriesData {
        getCountriesData {
            country
            infected
            recovered
            deceased
        }
    }
`;

export const GET_FAV = gql`
    query GetFav($uid: String!) {
        getFav(uid: $uid) {
            country
        }
    }
`;

export const GET_INDIA_DATA = gql`
    query getIndiaData {
        getIndiaData {
            state
            infected
            recovered
            deceased
        }
    }
`;