import { Route, Switch } from "react-router-dom";
import './App.css';
import Home from "./containers/Home";
import Country from "./containers/Country";
import State from "./containers/State";
import Login from "./containers/Login/Login";
import Signup from "./containers/Signup/Signup";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/signup" component={Signup}/>
        <Route exact path="/:country" component={Country}/>
        <Route exact path="/:country/:state" component={State}/>
      </Switch>
    </div>
  );
}

export default App;
