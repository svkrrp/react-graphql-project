const firebase = require("../../utils/firebase");

const signupUserToDb = ({email, password}) => new Promise((resolve, reject) => {
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(user => {
        resolve(user.user.uid);
    })
    .catch(err => {
        reject(err);
    })
});

const verifyUserFromDb = ({email, password}) => new Promise((resolve, reject) => {
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then((user) => {
        resolve(user.user.uid);
    })
    .catch(err => {
        reject(err);
    })
});

const setFavToDb = ({ uid, type, value }) => new Promise((resolve, reject) => {
    firebase.firestore().collection("users").doc(uid).set({
        [type]: value
    }).then((res) => {
        resolve('success');
    }).catch(err => {
        reject('error')
    }) 
})

const Mutation = {
    signupUser(parent, {data}, ctx, info) {
        return signupUserToDb(data);
    },
    loginUser(parent, {data}, ctx, info) {
        return verifyUserFromDb(data);
    },
    setFav(parent, { data }, ctx, info) {
        return setFavToDb(data);
    }
};

module.exports = Mutation;