const axios = require("axios");
const firebase = require("../../utils/firebase");

const getData = () => new Promise((resolve, reject) => {
    axios.get("https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true")
    .then((res) => {
        res.data.forEach(country => {
            country.infected === "NA" ? country.infected = null: '';
            country.recovered === "NA" ? country.recovered = null: '';
            country.deceased === "NA" ? country.deceased = null: '';
        });
        res.data.sort((a, b) => {
            if(a.infected > b.infected)
                return -1;
            else 
                return 1;
        });
        resolve(res.data);
    })
    .catch(err => {
        reject(err);
    });
});

const fetchWorldData = () => new Promise((resolve, reject) => {
    axios.get("https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true")
    .then((res) => {
        res.data.forEach(country => {
            country.infected === "NA" ? country.infected = 0: '';
            country.recovered === "NA" ? country.recovered = 0: '';
            country.deceased === "NA" ? country.deceased = 0: '';
        });
        const result = {
            total: 0,
            infected: 0,
            recovered: 0,
            deceased: 0
        };
        res.data.forEach(c => {
            result.infected += c.infected;
            result.recovered += c.recovered;
            result.deceased += c.deceased;
        })
        result.total = result.infected - (result.recovered + result.deceased);
        resolve(result);
    })
    .catch(err => {
        reject(err);
    });
});

const fetchCountryData = (countryName) => new Promise((resolve, reject) => {
    axios.get("https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true")
    .then((res) => {
        res.data.forEach(country => {
            country.infected === "NA" ? country.infected = null: '';
            country.recovered === "NA" ? country.recovered = null: '';
            country.deceased === "NA" ? country.deceased = null: '';
        });
        const getCountry = res.data.filter(country => country.country === countryName)[0];
        getCountry.total = getCountry.infected - (getCountry.recovered + getCountry.deceased);
        resolve(getCountry);
    })
    .catch(err => {
        reject(err);
    });
});

const fetchIndiaData = () => new Promise((resolve, reject) => {
    axios.get('https://api.rootnet.in/covid19-in/stats')
    .then(res => {
        const data = res.data.data.regional;
        data.forEach(r => {
            r.state = r.loc;
            r.infected = r.totalConfirmed;
            r.recovered = r.discharged;
            r.deceased = r.deaths;
        });
        resolve(data);
    }).catch(err => {
        reject(err);
    })
});

const fetchStateData = (stateName) => new Promise((resolve, reject) => {
    axios.get('https://api.rootnet.in/covid19-in/stats')
    .then(res => {
        const data = res.data.data.regional;
        data.forEach(r => {
            r.state = r.loc;
            r.infected = r.totalConfirmed;
            r.recovered = r.discharged;
            r.deceased = r.deaths;
        });
        const getState = data.filter(s => s.state === stateName)[0];
        getState.total = getState.infected - (getState.recovered + getState.deceased);
        resolve(getState);
    }).catch(err => {
        reject(err);
    })
});

const getFavFromDb = (uid) => new Promise((resolve, reject) => {
    firebase.firestore().collection("users").doc(uid).get()
    .then(function(doc) {
        if(doc.exists) {
            console.log(doc.data());
            resolve(doc.data());
        } else {
            reject(err);
        }
    }).catch(err => {
        reject(err);
    })
});

const Query = {
    user() {
        return {
            email: "s@gmail.com",
            password: "123"
        }
    },
    getWorldData() {
        return fetchWorldData();
    },
    getCountriesData() {
        return getData();
    },
    getCountryData(parent, {country}, ctx, info) {
        return fetchCountryData(country);
    },
    getIndiaData() {
        return fetchIndiaData();
    },
    getStateData(parent, {state}, ctx, info) {
        return fetchStateData(state);
    },
    getFav(parent, {uid}, ctx, info) {
        return getFavFromDb(uid);
    }
}

module.exports = Query;