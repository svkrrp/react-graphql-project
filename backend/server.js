const { GraphQLServer } = require('graphql-yoga');

const Query = require('./src/resolvers/Query');
const Mutation = require('./src/resolvers/Mutation');

const resolvers = {
  Query, // read
  Mutation // create, update, delete
};

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers
});

server.start(() => {
  console.log('server is up');
});
