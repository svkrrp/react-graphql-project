const firebase = require('firebase/app');
require('firebase/auth');
require('firebase/firestore');

const firebaseConfig = {
  apiKey: "AIzaSyDAHj4JdHWwghRixjAkwgq99qQpPq7j6Mc",
  authDomain: "react-graphql-app.firebaseapp.com",
  projectId: "react-graphql-app",
  storageBucket: "react-graphql-app.appspot.com",
  messagingSenderId: "269329405105",
  appId: "1:269329405105:web:2178a8cd18c0d57fe67e90"
};

firebase.initializeApp(firebaseConfig);

module.exports = firebase;
